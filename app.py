from flask import Flask, jsonify, render_template, request
import requests
from datetime import datetime

# Helper function
from functions import *

app = Flask(__name__)

####API####

# Steam Web API

# Key: 84B40D793432D4FA4A0E8E046DF3FF89

# User ID Mikreen: 76561198124005799, Reatox : 76561198131167895, Unknown : 76561198018480668

# Domain Name: localhost

# Recently played games Test request: http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key=84B40D793432D4FA4A0E8E046DF3FF89&steamid=76561198124005799&format=json

# Steamdocs: https://developer.valvesoftware.com/wiki/Steam_Web_API#GetNewsForApp_.28v0001.29

# User made docs for steam : https://steamapi.xpaw.me/

# Other steam API : https://steamapis.com/docs/market#apps

# API de open critic, meta critic existe

# https://api.opencritic.com/api/game/8485

# Docs for steamspy : https://steamspy.com/api.php

# Get the top 100 games the last 2 weeks : steamspy.com/api.php?request=top100in2weeks or https://openbase.io/js/steamspy-api/documentation


# Twitch API Keys: Client id : 7vk5gz6sccmyeqfcfkupjswcxpunrf
# Client secret : m90ahhj75odqkmcsy03u7ymi5qhiqa
# To receive the access token :
# https://id.twitch.tv/oauth2/token?client_id=7vk5gz6sccmyeqfcfkupjswcxpunrf&client_secret=m90ahhj75odqkmcsy03u7ymi5qhiqa&grant_type=client_credentials
# lrzp8v0ymeiiq0ljjrzlg5kydsc70j

# https://api-docs.igdb.com/#game


# MISC: url = "http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key=84B40D793432D4FA4A0E8E046DF3FF89&steamid=76561198124005799&format=json"
# response = requests.get(url)
# data = response.json()


# Function Jinja2 filter to transform datetime in seconds to a real date
@app.template_filter("timestamp_to_datetime")
def timestamp_to_datetime(timestamp):
    return datetime.utcfromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S")


def get_steam_data():
    domain = "http://api.steampowered.com"
    key = "84B40D793432D4FA4A0E8E046DF3FF89"


@app.route("/steam")
def numberOfPlayers():
    appid = "581320"
    url = (
        "http://api.steampowered.com/ISteamUserStats/GetNumberOfCurrentPlayers/v1/?appid="
        + appid
    )
    response = requests.get(url)
    data = response.json()

    return jsonify(data)


# Renders the home page
@app.route("/")
def home():
    return render_template("index.html")


# Refreshes the appList.json file
@app.route("/refresh", methods=["GET"])
def refresh():
    getAppList()
    data = open("static/jsonFiles/appList.json", "r")
    return data


@app.route("/refreshGameList", methods=["GET"])
def refreshGameList():
    getOnlyGames()
    data = open("static/jsonFiles/onlyGames.json", "r")
    return data


@app.route("/gameInfo", methods=["GET"])
def gameInfoPage():
    searchedGame = request.args.get("name")
    # Get the query parameters
    appid = getAppId(searchedGame)
    if appid != None:
        return render_template("game.html", appid=appid)  # , appInfo=appInfo)
    else:
        return render_template("error.html", error=appid), 404


##### API #####
@app.route("/api/<appid>/gameInfo", methods=["GET"])
def getGameInfo(appid):
    appInfo = searchGame(appid)
    return jsonify(appInfo)


@app.route("/api/search-game", methods=["POST"])
def search_game():
    searchedGame = request.form.get("search-bar")
    print(searchedGame)

    appid = getAppId(searchedGame)
    # appInfo = searchGame(appid)

    # Check if the game information is present and the request was successful
    print(appid)
    # if appInfo and appInfo.get("success", True):
    if appid != None:
        return render_template("game.html", appid=appid)  # , appInfo=appInfo)
    else:
        return render_template("error.html", error=appid), 404


# Returns the appid of the game
@app.route("/api/<appId>/metacritic", methods=["GET"])
def getMCScore(appId):
    score = getAppScore(appId)
    return jsonify(score)


@app.route("/api/<appId>/playerCount", methods=["GET"])
def getPlayerNumber(appId):
    playerNumber = getPlayerNumberGame(appId)
    return jsonify(playerNumber)


@app.route("/api/<appId>/genres", methods=["GET"])
def getGenres(appId):
    genres = getGameGenres(appId)
    return jsonify(genres)


@app.route("/api/<appId>/tags", methods=["GET"])
def getTags(appId):
    tags = getGameTags(appId)
    return jsonify(tags)


@app.route("/api/<appid>/gameName", methods=["GET"])
def gameName(appid):
    name = getGameName(appid)
    return jsonify(name)


# User search result page
@app.route("/api/search-user", methods=["POST"])
def search_user():
    searched_user = request.form.get("user-search-bar")
    print(searched_user)

    # Check if the requested username is empty
    if not searched_user:
        return render_template("error.html", error="Empty username"), 404

    steam_id = resolve_vanity_url(searched_user)
    if steam_id:
        user_info = getUserProfileInfo(steam_id)

        # Check if the response is not empty and valid JSON
        if not user_info or "response" not in user_info:
            return (
                render_template(
                    "error.html", error="User not found or invalid response"
                ),
                404,
            )

        # Check if the user account is present in the response
        players = user_info["response"].get("players")
        if not players or not players[0]:
            return render_template("error.html", error="User account not found"), 404

        return render_template(
            "profile.html",
            steam_id=steam_id,
            user_info=user_info,
        )
    else:
        return render_template("error.html", error="User not found"), 404


@app.route("/api/<userId>/recentGames", methods=["GET"])
def getRecentGames(userId):
    recentGames = getRecentlyPlayedGames(userId)
    return jsonify(recentGames)


@app.route("/api/<userId>/recentGenres", methods=["GET"])
def getRecentGameGenres(userId):
    recentGenres = getRecentGenres(userId)
    return jsonify(recentGenres)


@app.route("/api/<userId>/recentTags", methods=["GET"])
def getRecentGameTags(userId):
    tags = getRecentTags(userId)
    return jsonify(tags)


@app.route("/api/<userId>/ownedGames", methods=["GET"])
def getOwnedGames(userId):
    games = getAllOwnedGames(userId)
    return jsonify(games)


@app.route("/api/<userId>/topGames", methods=["GET"])
def getTopGames(userId):
    games = getTopGamesOfUser(userId)
    return jsonify(games)


@app.route("/api/<userId>/topTags", methods=["GET"])
def getTopTag(userId):
    tags = getTopTags(userId)
    return jsonify(tags)


@app.route("/api/<userId>/similarGames", methods=["GET"])
def similarGames(userId):
    games = getRecommendedGames(userId)
    return jsonify(games)


@app.route("/api/topGamesByPlayerNumber", methods=["GET"])
def topGamesByPlayerNumber():
    games = getTopGamesNumberUsers()
    return jsonify(games)


@app.route("/api/getTopReleases", methods=["GET"])
def topReleases():
    games = getTopReleases()
    return jsonify(games)


if __name__ == "__main__":
    app.run(debug=True)
