// Add event listener to refresh button
// Calls the refresh route on the server and adds the games as options for the search bar
document
  .querySelector(".refresh-button")
  .addEventListener("click", function () {
    refreshAllAppsList();
    fetch("/refreshGameList", { method: "GET" });
    fetch("static/jsonFiles/onlyGames.json")
      .then((response) => {
        if (!response.ok) {
          throw new Error("HTTP error " + response.status);
        }
        return response.json();
      })
      .then((data) => {
        if (!Array.isArray(data)) {
          console.error("Data is not an array:", data);
          return;
        }

        var dataList = document.querySelector("#games-list");

        // Clear current options
        dataList.innerHTML = "";

        // Add new options
        data.forEach(function (app) {
          var option = document.createElement("option");
          option.value = app.name;
          dataList.appendChild(option);
        });
      })
      .catch((error) => {
        console.error("Fetch error:", error);
      });
  });

async function refreshAllAppsList() {
  await fetch("/refresh", { method: "GET" });
}
