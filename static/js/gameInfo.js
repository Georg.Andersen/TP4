window.onload = function() {
    let appInfo = gameInfo();
}

async function gameInfo() {
    let gameInfoUrl = "/api/" + "{{ appid }}" + "/gameInfo";
    let playerCountUrl = "/api/" + "{{ appid }}" + "/playerCount";

    let currentPlayerCount = await fetch(playerCountUrl)
        .then((response) => response.json())
        .then((data) => {
            var playerCount = data.response.player_count;
            playerCount = playerCount.toLocaleString('fr-CH');
            return playerCount;
        })
    console.log(currentPlayerCount)

    await fetch(gameInfoUrl)
        .then((response) => response.json())
        .then((data) => {
            var appInfo = data["{{ appid }}"]["data"];
            var appInfoElement = document.querySelector(".content");

            try {
                appInfoElement.innerHTML += "<h1 class=\"game-type\" id=\"game-title\">" + appInfo.name + "</span></h1>";
                appInfoElement.innerHTML += "<p class=\"game-type\" id=\"game-type\">Type: <span class=\"bold\">" + appInfo.type + "</span></p>";

                if ('metacritic' in appInfo && 'score' in appInfo.metacritic) {
                    appInfoElement.innerHTML += "<p class=\"game-type\" id=\"score\">Score : <span class=\"bold\">" + appInfo.metacritic.score + "</span></p>";
                } else {
                    appInfoElement.innerHTML += "<p class=\"game-type\">Score : <span class=\"bold\">N/A</span></p>";
                }
                appInfoElement.innerHTML += "<p class=\"game-type\">Release Date: <span class=\"bold\">" + appInfo.release_date.date + "</span></p>";
                appInfoElement.innerHTML += "<p class=\"game-type\">Developers: <span class=\"bold\">" + appInfo.developers + "</span></p>";
                appInfoElement.innerHTML += "<p class=\"game-type\">Publishers: <span class=\"bold\">" + appInfo.publishers + "</span></p>";
                appInfoElement.innerHTML += "<img src=\"" + appInfo.header_image + "\" alt=\"Game Header Image\">";
                appInfoElement.innerHTML += "<p class=\"game-type\" id=\"player-count\">Player count: <span class=\"bold\">" + currentPlayerCount + "</span></p>";
                if ('price_overview' in appInfo && 'final_formatted' in appInfo.price_overview) {
                    appInfoElement.innerHTML += "<p class=\"game-type\">Price: <span class=\"bold\">" + appInfo.price_overview.final_formatted + "</span></p>";
                } else {
                    appInfoElement.innerHTML += "<p class=\"game-type\">Price: N/A</p>";
                }
                appInfoElement.innerHTML += "<p class=\"game-type\">About: <span class=\"bold\">" + appInfo.short_description + "</span></p>";
            } catch (err) {
                console.log(err);
            }
        });
}
