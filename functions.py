# This file contains all the useful and redundant functions which will call the apis

import requests
import json


# Docs https://steamapi.xpaw.me/
steamWebKey = "84B40D793432D4FA4A0E8E046DF3FF89"
steamId = "76561198018480668"
steamDomain = "http://api.steampowered.com"

numberRecentGames = 3
numberOfRecommendations = 10


#### Helper functions ####
# def sort_data(data):
#    sorted_data = dict(sorted(data.items(), key=lambda item: item[1], reverse=True))
#    return sorted_data


# This functions gets a list with all the apps that steam contains through the following format :
# {
#     "appid": 10,
#     "name": "Counter-Strike"
# }
# This functions takes a few seconds to run
def getAppList():
    url = steamDomain + "/ISteamApps/GetAppList/v2/" + "?key=" + steamWebKey
    response = requests.get(url)
    data = response.json()
    with open("static/jsonFiles/appList.json", "w") as outfile:
        json.dump(data, outfile)


def getOnlyGames(lastAppid=0):
    url = (
        steamDomain
        + "/IStoreService/GetAppList/v1/"
        + "?key="
        + steamWebKey
        + "&max_results=5000"
        + "&last_appid="
        + str(lastAppid)
    )
    response = requests.get(url)
    data = response.json()

    # Parse the data and only keep the appid and name
    parsed_data = [
        {"appid": app["appid"], "name": app["name"]} for app in data["response"]["apps"]
    ]

    # If the function is launched for the first time, delete the content of the file
    if lastAppid == 0:
        with open("static/jsonFiles/onlyGames.json", "w") as outfile:
            json.dump(parsed_data, outfile)
    else:
        # Load the existing data, append the new data, and write it back to the file
        with open("static/jsonFiles/onlyGames.json", "r+") as outfile:
            existing_data = json.load(outfile)
            outfile.seek(0)
            json.dump(existing_data + parsed_data, outfile)

    with open("static/jsonFiles/onlyGames.json", "r") as file:
        gamesss = json.load(file)

    # print("Number of items:", len(gamesss))

    if data["response"]["apps"] == None:
        return "No games found"
    if "have_more_results" not in data["response"]:
        return "All games found"
    if data["response"]["have_more_results"] == True:
        # print("Getting more games")
        getOnlyGames(data["response"]["last_appid"])


# This function searches the steam store with the appid and returns the result
def searchGame(appid):
    appid_str = str(appid)
    try:
        with open("static/jsonFiles/gameInfo.json", "r") as f:
            json_file = json.load(f)
            if appid_str not in json_file:
                # print("Game not cached")
                url = (
                    "https://store.steampowered.com/api/appdetails?appids=" + appid_str
                )
                response = requests.get(url)
                data = response.json()
                with open("static/jsonFiles/gameInfo.json", "w") as outfile:
                    json.dump(data, outfile)
                return data
            else:
                # print("Game cached, getting from file")
                return json_file
            # File doesn't exist
    except Exception as e:
        url = (
            "https://store.steampowered.com/api/appdetails?appids="
            + appid_str
            + "&l=english"
        )
        response = requests.get(url)
        data = response.json()
        with open("static/jsonFiles/gameInfo.json", "w") as outfile:
            json.dump(data, outfile)
        return data


def GetNumberOfCurrentPlayers(appid):
    url = (
        "http://api.steampowered.com/ISteamUserStats/GetNumberOfCurrentPlayers/v1/?appid="
        + appid
    )
    response = requests.get(url)
    data = response.json()
    return data


def getAppId(gameName):
    with open("static/jsonFiles/onlyGames.json") as json_file:
        data = json.load(json_file)
        # for app in data["applist"]["apps"]:  # This is for the appList.json file
        for app in data:  # This is for the onlyGames.json file
            if app["name"] == gameName:
                return app["appid"]
    with open("static/jsonFiles/appList.json") as json_file:
        data = json.load(json_file)
        for app in data["applist"]["apps"]:
            if app["name"] == gameName:
                return app["appid"]


def getGameName(appid):
    with open("static/jsonFiles/onlyGames.json", "r") as f:
        data = json.load(f)
        for game in data:
            if game["appid"] == appid:
                # print("Game found")
                return {"appid": appid, "name": game["name"]}
    # If the game is not found in the file, the app is probably a DLC, not a game
    # print("Game not found")
    # Hardcoded because the steam api does not return the name of the game each time. Problem on their side
    if appid == 271590:
        return {"appid": appid, "name": "Grand Theft Auto V"}
    data = searchGame(appid)
    for text in data:
        return {"appid": appid, "name": data[text]["data"]["name"]}


def getPlayerNumberGame(appid):
    url = (
        steamDomain
        + "/ISteamUserStats/GetNumberOfCurrentPlayers/v1/?appid="
        + str(appid)
    )
    response = requests.get(url)
    data = response.json()
    return data


# This function returns the metacritic score of the game by using the steam store api
# returns the appid and the score
def getAppScore(appid):
    data = searchGame(appid)
    for text in data:
        if data[text]["success"] == True:
            if "metacritic" not in data[text]["data"]:
                return {"appid": appid, "score": 0}
            return {"appid": appid, "score": data[text]["data"]["metacritic"]["score"]}


def getGameGenres(appid):
    data = searchGame(appid)
    for text in data:
        if data[text]["success"] == True:
            return {"appid": appid, "genres": data[text]["data"]["categories"]}


def getGameTags(appid):
    url = "https://steamspy.com/api.php?request=appdetails&appid=" + str(appid)
    response = requests.get(url)
    data = response.json()
    if data["name"] == "null":
        return "The game does not exist"
    # sorted_tags = sort_data(data["tags"])
    return data["tags"]


def getRecentlyPlayedGames(userId):
    url = (
        steamDomain
        + "/IPlayerService/GetRecentlyPlayedGames/v0001/?key="
        + steamWebKey
        + "&steamid="
        + userId
        + "&count="
        + str(numberRecentGames)
        + "&format=json"
    )
    response = requests.get(url)
    # print(response)
    if response.status_code != 200:
        return "The user does not exist"
    data = response.json()
    if data["response"]["total_count"] == 0:
        return "No games found"
    # print(data)
    return data


# This function returns a list of tags of the most recently played games
def getRecentGenres(userId):
    data = getRecentlyPlayedGames(userId)
    unwantedGenres = [
        "Steam Trading Cards",
        "Steam Achievements",
        "Steam Cloud",
        "Steam Workshop",
        "Steam Leaderboards",
        "HDR available",
        "Partial Controller Support",
        "Includes level editor",
        "In-App Purchases",
    ]
    genres = []
    for game in data["response"]["games"]:
        unparsedTags = getGameGenres(game["appid"])
        for tag in unparsedTags["tags"]:
            if (
                tag["description"] not in unwantedGenres
                and tag["description"] not in genres
            ):
                genres.append(tag["description"])
    return genres


def getRecentTags(userId):
    data = getRecentlyPlayedGames(userId)
    tags = []
    for game in data["response"]["games"]:
        unparsedTags = getGameTags(game["appid"])
        for tag in unparsedTags:
            if tag not in tags:
                tags.append(tag)
    return tags


def getTop100Games2Weeks():
    url = "https://steamspy.com/api.php?request=top100in2weeks"
    response = requests.get(url)
    data = response.json()
    return data


def getTop100GamesAllTime():
    url = "https://steamspy.com/api.php?request=top100forever"
    response = requests.get(url)
    data = response.json()
    return data


# returns all the owned and played games of a user
def getAllOwnedGames(userId):
    url = (
        "https://api.steampowered.com/IPlayerService/GetOwnedGames/v1/?key="
        + steamWebKey
        + "&steamid="
        + str(userId)
        + "&include_played_free_games=true&include_free_sub=true&language=English"
    )
    response = requests.get(url)
    data = response.json()
    games = []
    for game in data["response"]["games"]:
        if game["playtime_forever"] <= 0:
            data["response"]["games"].remove(game)
    for text in data["response"]["games"]:
        games.append({"appid": text["appid"], "playtime": text["playtime_forever"]})
    return games


# Gets the top n games of a user
def getTopGamesOfUser(userId):
    data = getAllOwnedGames(userId)

    # Sort the games by playtime in descending order
    sorted_games = sorted(data, key=lambda game: game["playtime"], reverse=True)

    # Get the top n games
    topGames = sorted_games[:numberRecentGames]

    return topGames


def getTopTags(userId):
    topGamesUser = getTopGamesOfUser(userId)
    tags = []
    for game in topGamesUser:
        unparsedTags = getGameTags(game["appid"])
        for tag in unparsedTags:
            if tag not in tags:
                tags.append(tag)
    return tags


def mostSimilarTagsGames(userTags, topGames):
    mostSimilarGames = []
    for game in topGames:
        gameTags = getGameTags(game)
        numberOfSimilarTags = 0
        for tag in gameTags:
            if tag in userTags:
                numberOfSimilarTags += 1
        if numberOfSimilarTags >= 10:
            mostSimilarGames.append(game)

    return mostSimilarGames


def removeOwnedGames(games, userGames):
    # Extract appids from userGames
    owned_appids = {str(userGame["appid"]) for userGame in userGames}
    
    # Create a new list of games that are not owned by the user
    new_games = [game for game in games if str(game) not in owned_appids]

    return new_games


def sortByScore(games):
    sorted_games = []
    for game in games:
        game_score = getAppScore(game)
        if game_score is not None:
            sorted_games.append(game_score)
    # print(sorted_games)
    # If the game doesn't have a score, then it will be -inf
    sorted_games = sorted(
        sorted_games, key=lambda game: game.get("score", float("-inf")), reverse=True
    )
    return sorted_games[:numberOfRecommendations]


# Find a steam user by his nickname
def resolve_vanity_url(vanity_url):
    url = (
        steamDomain
        + "/ISteamUser/ResolveVanityURL/v1/?key="
        + steamWebKey
        + "&vanityurl="
        + vanity_url
    )
    response = requests.get(url)
    data = response.json()
    if data["response"]["success"] == 1:
        return data["response"]["steamid"]
    else:
        return None


def getRecommendedGames(userId):
    # tags = getTopTags(userId)
    recent_tags = getRecentTags(userId)
    print(f"Recent Tags: {recent_tags}")

    topGames = getTop100Games2Weeks()

    games = mostSimilarTagsGames(recent_tags, topGames)
    print(f"Most Similar Tags Games: {games}")

    userGames = getAllOwnedGames(userId)
    print(f"All Owned Games: {userGames}")

    games = removeOwnedGames(games, userGames)
    print(f"Games after removing owned games: {games}")

    games = sortByScore(games)
    print(f"Games sorted by score: {games}")

    return games


# returns all the user profile info
def getUserProfileInfo(userId):
    url = (
        "https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v2/?key="
        + steamWebKey
        + "&steamids="
        + str(userId)
    )
    response = requests.get(url)
    data = response.json()
    return data


def getTopGamesNumberUsers():
    url = (
        steamDomain
        + "/ISteamChartsService/GetGamesByConcurrentPlayers/v1/?key="
        + steamWebKey
    )
    response = requests.get(url)
    data = response.json()
    return data


def getTopReleases():
    url = (
        steamDomain + "/ISteamChartsService/GetTopReleasesPages/v1/?key=" + steamWebKey
    )
    response = requests.get(url)
    data = response.json()
    return data
