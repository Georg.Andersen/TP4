# Steam Game Recommendations

by Georg Andersen and Quentin Juilland

## Summary

- [Introduction](#introduction)
- [Motivation](#motivation)
- [Aim of the service](#aim-of-the-service)
- [Run](#run)
- [Functionnalities](#functionnalities)
- [Recommendations](#recommendations)
- [Project structure](#project-structure)
- [Architecture Schema](#architecture-schema)
- [API Documentation](#api-documentation)

## Introduction

The main purpose of this service is the recommendation of video games based upon the games played by a user and the best rated games on **_Metacritic_** (a review aggregation website).<br> Our service is based on **_Steam_**, the biggest video game distribution service and storefront.<br>
The full list of functionnalities are explained in [Functionnalities](#functionnalities).

This service is a mashup of serveral publicly available APIs and aggregates and filters the results.

To work on the project, we used **_Gitlab_**.

## Motivation

As we are both young adults who like videogames, we strongly believe that today's game industry is very flooded with a lot of content and that it is hard to know what games to play and if they are good or not.

![Alt text](SteamNewGames.png)
https://gameworldobserver.com/wp-content/uploads/2023/12/steam-games-by-year.jpg

Our aim is to provide an easy way for users to find games that they might be interested in. And let them access associated informations like the price of the game or it's grade to help him decide if he should buy it or even spend time playing it.

## Aim of the service

The use of this for someone would be to access steam related data.
For example the user could check the current pricing of a game that he is interested in.
He could also consult a certain user profile and see when this person was last connected.
He also would be able to ask for game recommendation based on the user's favorite games.

All of these let the user access a wide range of data and advise him on his futur choices regarding the videogame industry on pc.

## Run

To launch the program: run the **_app.py_** file. Alternatively, you can use `flask run`.
The application will run on your localhost at port 5000 or the next available port.
`http://127.0.0.1:5000`

The Steam API needs a key to operate. Included in this project is our key. For deployment, this key should be hidden.à

## Functionnalities:

- Search the informations of a video game such as:

  - The rating of the game
  - The price
  - The number of current players

- Search a user profile:
  - Get recommended games based on the games the user has previously played and the best rated games on _Metacritic_
  - Get basic profile info

## Recommendations:

The recommendation algorithm works as follows:

- Based on the owned games of the user, we get the game tags and compare them with the tags of the top 100 games on steam of the past 2 weeks
- The 10 games that have the highest number of similar tags and the ones with the highest Metacritic score will be recommended if the user hasn't played them

## Project structure:

The project is structured as follows:

- **_app.py_** contains the flask application, with the routes and the API endpoints
- **_functions.py_** contains the functions that are used to call the external APIs and the logic associated to the game recommendation
- **_static/_** contains the css files as well the jsonFiles
- **_templates/_** contains the html files

## Architecture Schema

Here you can find the architecture schema :
![Alt text](applicationArchitectureDiagram.png)

## Implementation Details

Implementation was done using Flask, HTML, CSS, JS, Jinja2. 
We used mltiple STEAM-related APIs. 

Our developent was made by running the service locally. 
We proceeded by an agile method (flexible and incremental) so that we could progress easily. We thought that it would be the best method for our type of working habits and work schedules.
We also used GitLab for code sharing and version control.

## API Documentation

### APIs used :

- Steam : http://api.steampowered.com/
  This is the official steam api, it is very well fournished but not so easy to manipulate.

- SteamSpy : https://steamspy.com/api.php
  This is a non-official api that lets us parse very well organised (unlike the official steam API) information about steam trending games as well as other useful ones.

- Steam Store : http://store.steampowered.com/api/
  This API is also from steam but lets us parse information that is more store-specific in the sense that it is more about the game than about all the other things that the whole steam services profides.

### 1. Get Game Information

**Route:** `/api/<appid>/gameInfo`  
**Method:** `GET`  
**Description:** Retrieve detailed information about a game specified by its `appid`.

---

### 2. Search for a Game

**Route:** `/api/search-game`  
**Method:** `POST`  
**Description:** Search for a game based on user input. Returns the HTML page displaying information about the game.

### 3. Get Metacritic Score

**Route:** `/api/<appId>/metacritic`  
**Method:** `GET`  
**Description:** Retrieve the Metacritic score for a game specified by its `appId`.

---

### 4. Get Player Count

**Route:** `/api/<appId>/playerCount`  
**Method:** `GET`  
**Description:** Retrieve the current player count for a game specified by its `appId`.

---

### 5. Get Game Genres

**Route:** `/api/<appId>/genres`  
**Method:** `GET`  
**Description:** Retrieve the genres associated with a game specified by its `appId`.

---

### 6. Get Game Tags

**Route:** `/api/<appId>/tags`  
**Method:** `GET`  
**Description:** Retrieve the tags associated with a game specified by its `appId`.

---

### 7. Get Game Name

**Route:** `/api/<appid>/gameName`  
**Method:** `GET`  
**Description:** Retrieve the name of a game specified by its `appid`.

---

### 8. Search for a User

**Route:** `/api/search-user`  
**Method:** `POST`  
**Description:** Search for a Steam user based on the provided username. Returns the HTML page displaying user information.

---

### 9. Get Recent Games of a User

**Route:** `/api/<userId>/recentGames`  
**Method:** `GET`  
**Description:** Retrieve the list of recent games played by a user specified by their `userId`.

---

### 10. Get Genres of User's Recent Games

**Route:** `/api/<userId>/recentGenres`  
**Method:** `GET`  
**Description:** Retrieve the genres of the recent games played by a user specified by their `userId`.

---

### 11. Get Tags of User's Recent Games

**Route:** `/api/<userId>/recentTags`  
**Method:** `GET`  
**Description:** Retrieve the tags of the recent games played by a user specified by their `userId`.

---

### 12. Get All Owned Games of a User

**Route:** `/api/<userId>/ownedGames`  
**Method:** `GET`  
**Description:** Retrieve the list of all games owned by a user specified by their `userId`.

---

### 13. Get Top Games of a User

**Route:** `/api/<userId>/topGames`  
**Method:** `GET`  
**Description:** Retrieve the top games of a user specified by their `userId`.

---

### 14. Get Top Tags of a User

**Route:** `/api/<userId>/topTags`  
**Method:** `GET`  
**Description:** Retrieve the top tags associated with games played by a user specified by their `userId`.

---

### 15. Get Similar Games for a User

**Route:** `/api/<userId>/similarGames`  
**Method:** `GET`  
**Description:** Retrieve a list of games recommended for a user specified by their `userId`.

---

### 16. Get Top Games by Player Number

**Route:** `/api/topGamesByPlayerNumber`  
**Method:** `GET`  
**Description:** Retrieve a list of top games based on player count.

---

### 17. Get Top Releases

**Route:** `/api/getTopReleases`  
**Method:** `GET`  
**Description:** Retrieve a list of top game releases.

---

## Conclusion

### What we developped : 

We developped (amongst other things) :

Web display
- Game recommendation
- User and game information

Custom API
- have the appid of a game
- have a user's games
- have game recommendations for a user


The service we developped works pretty well in our sense but there is things that could still be done to improve our work.


For example :
We could have added better security in by example protecting against code injection.
We also could have added more features, like getting similar games on a game page to or displaying the friends of a specific user to go see their profiles and maybe what games they could like.
The code is alread pretty factorized but we could have put the javascripts in every html in separate files.
Finally, we could have cached some requests for example the top 100 best games in the 2 last weeks with their respective tags each so that we have less api calls and thus less time for our game recommandation function.

Our overall satisfaction of the project is pretty great.
We think our project is functional and we succeeded in developping what we initially wanted. We believe we have satisfatory results.

